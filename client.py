import socket, pickle
import _thread as thread


class Client():
    def __init__(self, host, port, username):
        try:
            self.host = host
            self.port = port
            self.s = socket.socket()
            self.username = username
        except socket.error as msg:
            print(str(msg))

    def main(self):
        self.connect()
        thread.start_new_thread(self.listening_thread,())
        while True:
            text = input("{0}: ".format(self.username))
            self.send_msg(text)

    def connect(self):
        self.s.connect((self.host, self.port))

    def send_msg(self, text):
        if text == "exit":
            self.s.close()
        elif text == "":
            pass
        else:
            self.s.send(pickle.dumps([self.username, text]))

    def recieve_data(self):
            data = self.s.recv(1024)
            return (data.decode("utf-8"))

    def listening_thread(self)
        while True:
            print("\n{0}".format(self.recieve_data()))


if __name__ == "__main__":
    server = input("server: ")
    port = input("port: ")
    username = input("username: ")
    client = Client(server, int(port), username)
    client.main()
